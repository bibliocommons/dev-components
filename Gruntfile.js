// Generated on 2014-06-20 using generator-webapp 0.4.9
'use strict';

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. 
    require('time-grunt')(grunt);

    // Configurable paths
    var config = {
        app: 'app',         // html, tpls
        assets: 'assets',   // images, js, scripts, etc
        build: 'build',     // generated site
    };

    //
    // Tasks for local dev & prod
    // 
    grunt.registerTask('serve', function (target) {

        grunt.task.run([
          'clean:server',
          'concurrent',
          'autoprefixer',
          'includes:server',
          'connect:livereload',
          'watch'
        ]);
    });
    


    //
    // Define the configuration for all the tasks
    //
    grunt.initConfig({

      // Project settings
      config: config,

      //
      // ============== WATCH
      // 
      watch: {
        gruntfile: {
          files: ['Gruntfile.js']
        },
        js: {
          files: ['<%= config.assets %>/scripts/{,*/}*.js'],
          tasks: ['jshint'],
          options: {
              livereload: true
          }
        },
        sass: {
          files: ['<%= config.assets %>/styles/{,*/}*.{scss,sass}'],
          tasks: ['sass:server', 'autoprefixer']
        },

        livereload: {
          options: {
              livereload: '<%= connect.options.livereload %>'
          },
          files: [
            '<%= config.app %>/includes/*.tpl',
            '<%= config.app %>/*.html',
            '<%= config.assets %>/styles/{,*/}*.scss',
            '<%= config.assets %>/scripts/{,*/}*.js'
          ],
          tasks: ['includes:server']
        }
      },

      //
      // ============== TASKS
      // 
      connect: {
        options: {
          port: 9000,
          open: true,
          livereload: 35729,
          hostname: 'localhost'
        },
        livereload: {
          options: {
            middleware: function(connect) {
              return [
                // connect any links from index.html to /bower_components 
                // to actual root bower component folder
                connect().use('/bower_components', connect.static('./bower_components')),
                connect.static(config.build)
              ];
            }
          }
        },
        dist: {
          options: {
            base: '<%= config.build %>',
            livereload: false
          }
        }
      },


       // Clears folders to start fresh
      clean: {
        server:  [
          '<%= config.build %>',
          '.sass-cache'
        ],
        dist: {
          files: [{
              dot: true,
              src: [
                  '<%= config.dist %>/*',
                  '!<%= config.dist %>/.git*'
              ]
          }]
        }            
      },

      // Run some (slow) tasks in parallel to speed up process
      concurrent: {
        server: [
          'sass',
          'copy'
        ]
      },

      // Compiles Sass to CSS and generates necessary files if requested
      sass: {
        options: {
            loadPath: [
              'bower_components'
            ]
        },
        server: {
            files: [{
                expand: true,
                cwd: '<%= config.assets %>/styles',
                src: ['*.scss'],
                dest: '<%= config.build %>/styles',
                ext: '.css'
            }]
        }
      },
      // Copies remaining files to places other tasks can use
      copy: {
        server: {
          files: [
            { // root folder 
              expand: true,
              dot: true,
              cwd: '<%= config.assets %>/root',
              dest: '<%= config.build %>',
              src: [
                  '*.{ico,txt}',
                  '{,*/}*.html',
                  '.htaccess'
              ]
            },
            { // images
              expand: true,
              dot: false,
              cwd: '<%= config.assets %>/images',
              dest: '<%= config.build %>/images',
              src: ['{,*/}*.*']
            },
            { // generic fonts
              expand: true,
              dot: true,
              cwd: '<%= config.assets %>/styles/fonts',
              dest: '<%= config.build %>/styles/fonts',
              src: ['{,*/}*.*']
            },
            { // vendor css
              expand: true,
              dot: true,
              cwd: '<%= config.assets %>/styles/vendor',
              dest: '<%= config.build %>/styles/vendor',
              src: ['{,*/}*.*']
            },
            { // bootstrap's font
              expand: true,
              dot: true,
              cwd: 'bower_components/bootstrap-sass-official/vendor/assets/fonts/bootstrap',
              src: ['*.*'],
              dest: '<%= config.build %>/styles/fonts'
            }, 
            { // js
              expand: true,
              dot: true,
              cwd: '<%= config.assets %>/scripts',
              dest: '<%= config.build %>/scripts',
              src: ['{,*/}*.js*']
            }
          ]
        }
      },

      // Add vendor prefixed styles (on already generated file)
      autoprefixer: {
        options: {
          browsers: ['last 1 version']
        },
        server: {
          files: [{
              expand: true,
              cwd: '<%= config.build %>/styles/',
              src: '{,*/}*.css',
              dest: '<%= config.build %>/styles/'
          }]
        }
      },

      // Include html within html 
      includes: {
        server: {
          cwd: '<%= config.app %>',
          src: [ '*.html', 'components/*.html'],
          dest: '<%= config.build %>',
          options: {
            flatten: true,
            includePath: '<%= config.app %>/includes',
            banner: '<!-- Generated with grunt -->\n'
          }
        }
      },

      // Make sure code styles are up to par and there are no obvious mistakes
      jshint: {
        options: {
          jshintrc: '.jshintrc',
          reporter: require('jshint-stylish')
        },
        all: [
          'Gruntfile.js',
          '<%= config.assets %>/scripts/{,*/}*.js'
        ]
      }

    }); //end initConfig

};