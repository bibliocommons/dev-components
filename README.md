# README #

## What is this repository for? ##

* Managing the list of UI components for CORE (and/or others)
* Version 0.1

## How do I get set up? ##

### Install NPM ###


```
#!bash

brew install npm
```

### Install SASS ###

Grunt compiles sass files with https://github.com/gruntjs/grunt-contrib-sass. 

Install so this works: http://sass-lang.com/install


```
#!bash

gem install sass
```

## Running The App ##

On the root directory, run:

```
#!bash

grunt serve
```

It's successful if you see "Done without errors". And at the bottom it says "Waiting..."

## Notes ##

* Keep an eye on the output of the gruntfile, it'll tell you if there are issues with your code 
* Check out the Gruntfile.js to see all the tasks that are setup. 
* Notice we use livereload


## Developing ##

* Add new html/tpl files inside /app
* Add new assets (not bower controlled) in the /assets folder
* When a new file is added to /app, /assets, you must restart "grunt serve"
* The app that you are viewing on your local browser on is the /build folder

## Deploying ##

Hasn't been figuered out yet. We'll have to publish the build folder. But first we'll need to minimize all the bower components.