      <footer>
        <p><span class="glyphicon glyphicon-copyright-mark"></span> Bibliocommons</p>
      </footer>
    </div>

    <!-- build:js /scripts/plugins.js -->
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>

    <script src="/bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/affix.js"></script>
    <script src="/bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/alert.js"></script>
    <script src="/bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/dropdown.js"></script>
    <script src="/bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/tooltip.js"></script>
    <script src="/bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/modal.js"></script>
    <script src="/bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/transition.js"></script>
    <script src="/bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/button.js"></script>
    <script src="/bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/popover.js"></script>
    <script src="/bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/carousel.js"></script>
    <script src="/bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/scrollspy.js"></script>
    <script src="/bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/collapse.js"></script>
    <script src="/bower_components/bootstrap-sass-official/vendor/assets/javascripts/bootstrap/tab.js"></script>
    <!-- endbuild -->

    <script src="/scripts/vendor/prism.js"></script>
    
    <script src="/scripts/main.js"></script>
</body>
</html>
