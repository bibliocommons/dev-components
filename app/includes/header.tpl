<!doctype html>
<html class="no-js">
  <head>
      <meta charset="utf-8">
      <title>DevComponents - Bibliocommons</title>
      <meta name="description" content="List of dev components for bibliocommons CORE.">
      <meta name="viewport" content="width=device-width">
      <link rel="shortcut icon" href="/favicon.ico">
      <link rel="stylesheet" href="/styles/vendor/prism.css" />
      <link rel="stylesheet" href="/styles/main.css">
  </head>
  <body>
       
    <div class="container">
      <header>
        <nav class="navbar navbar-default" role="navigation">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="/">DevComponents List</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#">Components</a></li>
                <li><a href="https://bitbucket.org/bibliocommons/dev-components" target="_blank">Repo</a></li>
                
              </ul>
              
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
			</header>